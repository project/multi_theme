<?php

namespace Drupal\multi_theme\Controller;

use Drupal\Core\Url;
use Drupal\system\Controller\SystemController as CoreSystemController;

/**
 * Returns responses for System routes.
 */
class SystemController extends CoreSystemController {

  /**
   * {@inheritdoc}
   */
  public function themesPage() {
    $build = parent::themesPage();

    $additional_default = $this->config('system.theme')->get('additional_default') ?? [];
    /** @var \Drupal\Core\Extension\Extension $theme */
    foreach($build[0]['#theme_groups']['installed'] as $theme) {
      if (!$theme->is_admin && !$theme->is_default) {
        if (!in_array($theme->getName(), $additional_default)) {
          $theme->operations[] = [
            'title' => $this->t('Set as additional default'),
            'url' => Url::fromRoute('multi_theme.manage_additional_default_theme'),
            'query' => ['theme' => $theme->getName(), 'mode' => 'add'],
            'attributes' => ['title' => $this->t('Set @theme as additional default theme', ['@theme' => $theme->info['name']])],
          ];
        }
        else {
          $theme->notes[] = $this->t('additional default theme');

          $theme->operations[] = [
            'title' => $this->t('Unset as additional default'),
            'url' => Url::fromRoute('multi_theme.manage_additional_default_theme'),
            'query' => ['theme' => $theme->getName(), 'mode' => 'remove'],
            'attributes' => ['title' => $this->t('Unset @theme as additional default theme', ['@theme' => $theme->info['name']])],
          ];
        }
      }
    }
    return $build;
  }

}
