<?php

namespace Drupal\multi_theme\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\AjaxBasePageNegotiator as CoreAjaxBasePageNegotiator;

/**
 * Defines a theme negotiator that deals with multiple active themes on ajax.
 */
class AjaxBasePageNegotiator extends CoreAjaxBasePageNegotiator {

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $ajax_page_state = $this->requestStack->getCurrentRequest()->request->get('ajax_page_state');
    $theme = $ajax_page_state['theme'];

    // Prevent a request forgery from giving a person access to a theme they
    // shouldn't be otherwise allowed to see. However, since everyone is
    // allowed to see the default theme or the secondary default themes, token
    // validation isn't required for that, and bypassing it allows most
    // use-cases to work even when accessed from the page cache.
    $config = $this->configFactory->get('system.theme');
    if (in_array($theme, (array) $config->get('additional_default'), TRUE)) {
      return $theme;
    }
  }

}
