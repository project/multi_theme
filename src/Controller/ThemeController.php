<?php

namespace Drupal\multi_theme\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\system\Controller\ThemeController as CoreThemeController;

/**
 * Controller for theme handling.
 */
class ThemeController extends CoreThemeController {

  /**
   * Manage the additional default theme.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request object containing a theme name.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|array
   *   Redirects back to the appearance admin page or the confirmation form
   *   if an experimental theme will be installed.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Throws access denied when no theme is set in the request.
   */
  public function manageAdditionalDefaultTheme(Request $request) {
    $config = $this->configFactory->getEditable('system.theme');
    $theme = $request->query->get('theme');
    $mode = $request->query->get('mode');
    $add = $mode === 'add' ? TRUE : FALSE;
    $remove = $mode === 'remove' ? TRUE : FALSE;

    if (isset($theme) && ($add || $remove)) {
      // Get current list of themes.
      $themes = $this->themeHandler->listInfo();
      // Check if the specified theme is one recognized by the system.
      if (isset($themes[$theme])) {
        $additional_default_themes = $config->get('additional_default') ?? [];

        // Add or remove the theme as additional theme.
        if ($add) {
          $additional_default_themes[] = $theme;
        }
        elseif ($remove) {
          $additional_default_themes = array_diff($additional_default_themes, [$theme]);
        }

        $additional_default_themes = array_values(array_unique($additional_default_themes));
        $config->set('additional_default', $additional_default_themes)->save();
      }
      else {
        $this->messenger()->addError($this->t('The %theme theme was not found or mode %mode is not recognized.', ['%theme' => $theme, '%mode' => $mode]));
      }

      return $this->redirect('system.themes_page');
    }
    throw new AccessDeniedHttpException();
  }

}
