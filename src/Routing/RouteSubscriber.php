<?php

namespace Drupal\multi_theme\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $route = $collection->get('system.themes_page');
    $route->setDefault('_controller', 'Drupal\multi_theme\Controller\SystemController::themesPage');
  }

}
